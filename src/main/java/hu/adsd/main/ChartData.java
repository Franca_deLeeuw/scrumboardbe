package hu.adsd.main;

/*
* Class for an Object representing a data point used in a burndown chart
* storyPointsTotal is the total amount of storypoints in current sprint
* storyPointsDone is the amount of storypoints of user stories that have been Done this sprint
* storyPointsDate is the date at witch the data point was created
* */
public class ChartData
{
    private int storyPointsTotal;
    private int storyPointsDone;
    private String storyPointsDate;

    public ChartData(int storyPointsTotal, int storyPointsDone, String storyPointsDate) {
        this.storyPointsTotal = storyPointsTotal;
        this.storyPointsDone = storyPointsDone;
        this.storyPointsDate = storyPointsDate;
    }

    public int getStoryPointsTotal() {
        return storyPointsTotal;
    }

    public void setStoryPointsTotal(int storyPointsTotal) {
        this.storyPointsTotal = storyPointsTotal;
    }

    public int getStoryPointsDone() {
        return storyPointsDone;
    }

    public void setStoryPointsDone(int storyPointsDone) {
        this.storyPointsDone = storyPointsDone;
    }

    public String getStoryPointsDate() {
        return storyPointsDate;
    }

    public void setStoryPointsDate(String storyPointsDate) {
        this.storyPointsDate = storyPointsDate;
    }
}
