package hu.adsd.main;

import com.sun.net.httpserver.HttpServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
/*
* This is the main class of this application and the only class that should be run to start the REST API service
* */
@SpringBootApplication
public class MainApplication
{
	public static void main (String[]args) throws IOException
	{
		// While the REST API is running, check every minute to see if it is 00:00 on the user's PC
		// If true, create a data point for the burndown chart using DatabaseReader
		DatabaseReader databaseReader = new DatabaseReader();
		Timer timer = new Timer();
		TimerTask updateChartDataCheck = new TimerTask()
		{
			@Override
			public void run()
			{
				String updateTime = "00:00";
				DateFormat datumFormat = new SimpleDateFormat("HH:mm");
				Date currentTime = new Date();
				String stringcurrentTime = datumFormat.format(currentTime);
				if (stringcurrentTime.equals(updateTime))
				{
					databaseReader.checkChartData();
				}
			}
		};
		timer.scheduleAtFixedRate(updateChartDataCheck, 0, 60000);

		//Start the Rest API service
		SpringApplication.run(MainApplication.class, args);
	}
}

