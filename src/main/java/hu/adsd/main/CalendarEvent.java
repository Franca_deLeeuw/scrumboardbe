package hu.adsd.main;

import com.google.api.client.util.DateTime;

/*
* Class used for objects that are an appointment in a calender
* summary is the title of the appointment
* dateTime is the date and time of the appointment
* desc is the description of the appointment.
*/
public class CalendarEvent {

    private String summary;
    private DateTime dateTime;
    private String desc;

    public CalendarEvent(String summary, DateTime dateTime, String desc) {
        this.summary = summary;
        this.dateTime = dateTime;
        this.desc = desc;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public DateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(String desc) {
        this.dateTime = dateTime;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
