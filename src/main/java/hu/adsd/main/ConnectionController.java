package hu.adsd.main;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/*
* Class that handles all the requests our REST API gets.
* API listens to http://localhost:8080
* */
@RestController
public class ConnectionController
{
    // Gets the amount of userstories per column from the trello API
    @CrossOrigin(origins = "*")
    @GetMapping("/usamount")
    public ArrayList<UserStoryColumn> getAmountofUserStories()
    {
        TrelloApiHandler trelloApiHandler = new TrelloApiHandler();
        return trelloApiHandler.UserStoryAmount(trelloApiHandler.getIssuesFromAPI());
    }

    // Gets the team's schedule from ExcelSheet.xlsx
    @CrossOrigin(origins = "*")
    @GetMapping("/scedule")
    public String[][] getScedule()
    {
        //Create date object
        Date date = new Date();
        // object of the class
        ExcelReader excelReader = new ExcelReader();
        // reading the value of the cells
        String[][] outputScedule = excelReader.Scedule(0,1,2,3,4,5,6,0, date.getDay());
        return  outputScedule;
    }

    // Gets the name of the employee of the Sprint from ExcelSheet.xlsx
    @CrossOrigin(origins = "*")
    @GetMapping("/employeeofsprintname")
    public String getEmployeeOfSprintName()
    {
        //object of the class
        ExcelReader excelReader = new ExcelReader();
        //reading the value of the cell
        String employeeOfTheSprintName = excelReader.EmployeeOfTheSprintName(18,2);
        return employeeOfTheSprintName;
    }

    // Gets the total storypoints done and total amount of hours worked by the employee of the sprint
    // from ExcelSheet.xlsx
    @CrossOrigin(origins = "*")
    @GetMapping("/employeeofsprintinfo")
    public ArrayList<Double> getEmployeeOfSprintInfo()
    {
        //object of the class
        ExcelReader excelReader = new ExcelReader();
        //reading the value of the cell
        ArrayList<Double> employeeOfTheSprintInfo = excelReader.EmployeeOfTheSprintInfo(16,17,2);
        return employeeOfTheSprintInfo;
    }

    // Gets the names of all employees working on current sprint from ExcelSheet.xlsx
    @CrossOrigin(origins = "*")
    @GetMapping("/employees")
    public ArrayList<String> getEmployees()
    {
        //object of the class
        ExcelReader excelReader = new ExcelReader();
        //reading the value of the cell
        ArrayList<String> employees = excelReader.Employees(2,3,4,5,6,7,4);
        return employees;
    }

    // Gets all relevant information of all the userstories from the Trello API
    @CrossOrigin(origins = "*")
    @GetMapping("/userstorycard")
    public ArrayList<UserStoryCard> getUserStoryCard()
    {
        TrelloApiHandler trelloApiHandler = new TrelloApiHandler();
        return trelloApiHandler.UserStoryView();
    }

    // Gets all data points for use in the burndown chart from the database scrumboard.db
    @CrossOrigin(origins = "*")
    @GetMapping("/chartdata")
    public ArrayList<ChartData> getChartData()
    {
        DatabaseReader databaseReader = new DatabaseReader();
        return databaseReader.getChartDataFromDatabase();
    }

    // Gets all employee names that are connected to a user story per user story from the Trello API
    @CrossOrigin(origins = "*")
    @GetMapping("/cardmembers")
    public ArrayList<List<String>> getCardMembers()
    {
        TrelloApiHandler trelloApiHandler = new TrelloApiHandler();
        return trelloApiHandler.getCardNames();
    }

    // Gets the next 10 appointments from the Google Calendar API
    @CrossOrigin(origins = "*")
    @GetMapping("/calendar")
    public ArrayList<CalendarEvent> getCalendarFromApi()
    {
        ArrayList<CalendarEvent> calendarEventList = new ArrayList<>();
        CalendarQuickstart calendarQuickstart = new CalendarQuickstart();
        try
        {
            calendarEventList = calendarQuickstart.getCalendarEvents();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return calendarEventList;
    }
}
