package hu.adsd.main;

import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

/*
* This class is used to test 2 methodes of the TrelloApiHandler using JUnit
* */
class TrelloApiHandlerTest
{
    // The methode getChartFromApi is tested using TestJson
    @org.junit.jupiter.api.Test
    void getChartFromApi()
    {
        JSONParser jsonParser = new JSONParser();
        JSONArray jsonArray = new JSONArray();
        try
        {
            jsonArray = (JSONArray) jsonParser.parse(new FileReader("src/main/resources/TestJson"));
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        TrelloApiHandler trelloApiHandler = new TrelloApiHandler();

        assertEquals(trelloApiHandler.getChartFromApi(jsonArray).getStoryPointsTotal(), 314);
    }

    // The methode userStoryAmount is tested using TestJson
    @org.junit.jupiter.api.Test
    void userStoryAmount()
    {
        TrelloApiHandler trelloApiHandler = new TrelloApiHandler();

        JSONParser jsonParser = new JSONParser();
        JSONArray jsonArray = new JSONArray();
        try
        {
            jsonArray = (JSONArray) jsonParser.parse(new FileReader("src/main/resources/TestJson"));
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        assertEquals(trelloApiHandler.UserStoryAmount(jsonArray).get(4).getUserStoryAmount(), 7);
    }

}