package hu.adsd.main;

/*
* Object that represents a column within the scrumboard
* name is the name of the column
* userStoryAmount is the amount of user stories within the column
* */
public class UserStoryColumn
{
    private String name;
    private int userStoryAmount;

    public UserStoryColumn(String name, int userStoryAmount)
    {
        this.name = name;
        this.userStoryAmount = userStoryAmount;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public int getUserStoryAmount()
    {
        return userStoryAmount;
    }

    public void setUserStoryAmount(int userStoryAmount)
    {
        this.userStoryAmount = userStoryAmount;
    }
}
