package hu.adsd.main;

import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/*
* This Class handles all communication with the Trello API
* */
public class TrelloApiHandler
{
    // Gets the JSON data of all user stories on the dummy scrumboard openbaar from the Trello API
    public JSONArray getIssuesFromAPI()
    {
        JSONParser parser = new JSONParser();
        JSONArray array = new JSONArray();

        try
        {
            URL url = new URL("https://api.trello.com/1/boards/mPzQCgZb/lists?cards=all&key=8e35df159337734937bc8e9ceccb3d56");
            URLConnection urlConnection = url.openConnection();
            array = (JSONArray) parser.parse(new InputStreamReader(urlConnection.getInputStream()));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return array;
    }

    // Gets the JSON data of all members on the dummy scrumboard openbaar from the Trello API
    public JSONArray getMembersFromAPI()
    {
        JSONParser parser = new JSONParser();
        JSONArray array = new JSONArray();

        try
        {
            URL url = new URL("https://api.trello.com/1/boards/mPzQCgZb/members?key=8e35df159337734937bc8e9ceccb3d56");
            URLConnection urlConnection = url.openConnection();
            array = (JSONArray) parser.parse(new InputStreamReader(urlConnection.getInputStream()));
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return array;
    }

    // Gets the data of one data point for the burndown chart from a JSONArray
    public ChartData getChartFromApi(JSONArray array)
    {
        int totalStoryPoints = 0;
        int storyPointsDone = 0;

        try
        {
            for (Object columnObject : array)
            {
                JSONObject finalColumnObject = (JSONObject) columnObject;

                JSONArray cardArray = (JSONArray) finalColumnObject.get("cards");

                for (Object cardObject : cardArray)
                {
                    JSONObject finalCardObject = (JSONObject) cardObject;

                    JSONArray labelArray = (JSONArray) finalCardObject.get("labels");

                    for (Object labelObject : labelArray)
                    {
                        JSONObject finalLabelObject = (JSONObject) labelObject;

                        if (finalLabelObject.get("color").equals("black"))
                        {
                            String storyPoints = (String) finalLabelObject.get("name");
                            totalStoryPoints += Integer.parseInt(storyPoints);
                        }
                    }
                }
                if (finalColumnObject.get("name").equals("Done"))
                {
                    for (Object cardObject : cardArray)
                    {
                        JSONObject finalCardObject = (JSONObject) cardObject;

                        JSONArray labelArray = (JSONArray) finalCardObject.get("labels");

                        for (Object labelObject : labelArray)
                        {
                            JSONObject finalLabelObject = (JSONObject) labelObject;
                            if (finalLabelObject.get("color").equals("black"))
                            {
                                String storyPoints = (String) finalLabelObject.get("name");
                                storyPointsDone += Integer.parseInt(storyPoints);
                            }
                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        String date = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
        ChartData chartData = new ChartData(totalStoryPoints, storyPointsDone, date);
        return chartData;
    }

    // Gets the amount of userstories per column from a JSONArray
    public ArrayList<UserStoryColumn> UserStoryAmount(JSONArray array)
    {
        {
            ArrayList<UserStoryColumn> columnList = new ArrayList<>();

            try
            {
                for (Object object : array)
                {
                    int userStoryAmount = 0;
                    JSONObject columnObject = (JSONObject) object;
                    JSONArray array1 = (JSONArray) columnObject.get("cards");
                    String progress = (String) columnObject.get("name");
                    for (Object object1 : array1)
                    {
                        userStoryAmount++;
                    }
                    columnList.add(new UserStoryColumn(progress, userStoryAmount));
                }

            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
            return columnList;
        }
    }

    // Gets all relevant information of all the userstories from getIssuesFromAPI
    public ArrayList<UserStoryCard> UserStoryView()
    {
        ArrayList<UserStoryCard> cardList = new ArrayList<>();

        try
        {
            JSONArray array = getIssuesFromAPI();

            for (Object object : array)
            {
                long criteriaAmount;
                long criteriaCheck;
                JSONObject columnObject = (JSONObject) object;
                JSONArray cardArray = (JSONArray) columnObject.get("cards");
                String progress = (String) columnObject.get("name");
                for (Object cardObject : cardArray)
                {
                    JSONObject finalObject = (JSONObject) cardObject;
                    String name = (String) finalObject.get("name");
                    JSONObject badgeObject = (JSONObject) finalObject.get("badges");
                    criteriaAmount = (long) badgeObject.get("checkItems");
                    criteriaCheck = (long) badgeObject.get("checkItemsChecked");
                    JSONArray labelArray = (JSONArray) finalObject.get("labels");
                    String link = (String) finalObject.get("shortUrl");
                    String storyPoints = "";
                    String priority = "";
                    for (Object labelObject : labelArray)
                    {
                        JSONObject finalLabelObject = (JSONObject) labelObject;
                        if (finalLabelObject.get("color").equals("black"))
                        {
                            storyPoints = (String) finalLabelObject.get("name");
                        }
                        else
                        {
                            priority = (String) finalLabelObject.get("name");
                        }
                    }
                    JSONArray idMemberArray = (JSONArray) finalObject.get("idMembers");

                    JSONArray memberArray = getMembersFromAPI();
                    ArrayList<String> names = new ArrayList<String>();

                    for (Object memberObject : memberArray)
                    {
                        JSONObject finalMemberObject = (JSONObject) memberObject;

                        String membersArray = (String) finalMemberObject.get("id");

                        for (Object finalMemberString : idMemberArray)
                        {
                            if (finalMemberString.equals(membersArray))
                            {
                                // add member name to small list of current card
                                names.add((String)finalMemberObject.get("fullName"));
                            }
                        }
                    }

                    UserStoryCard userStoryCard = new UserStoryCard(name, criteriaAmount, criteriaCheck, priority, Integer.parseInt(storyPoints), progress, names, link);
                    cardList.add(userStoryCard);
                }
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return cardList;
    }

    // Gets all employee names that are connected to a user story per user story from getMembersFromAPI
    public ArrayList<List<String>> getCardNames()
    {
        ArrayList<List<String>> cardNames = new ArrayList<>();

        try
        {
            JSONArray array = getIssuesFromAPI();

            for (Object columnObject : array)
            {
                JSONObject finalColumnObject = (JSONObject) columnObject;

                JSONArray cardArray = (JSONArray) finalColumnObject.get("cards");

                for (Object cardObject : cardArray)
                {
                    ArrayList<String> names = new ArrayList<String>();

                    JSONObject finalCardObject = (JSONObject) cardObject;

                    JSONArray memberIdArray = (JSONArray) finalCardObject.get("idMembers");
                    System.out.println("member id: " + memberIdArray);

                    JSONArray memberArray = getMembersFromAPI();

                    for (Object memberObject : memberArray)
                    {
                        JSONObject finalMemberObject = (JSONObject) memberObject;

                        String membersArray = (String) finalMemberObject.get("id");

                        for (Object finalMemberString : memberIdArray)
                        {
                            if (finalMemberString.equals(membersArray))
                            {
                                // add member name to small list of current card
                                names.add((String)finalMemberObject.get("fullName"));
                            }
                        }


                    }
                    // add all names of the members to the list (per card)
                    cardNames.add(names);
                }
            }

        } catch (Exception e)
        {
            e.printStackTrace();
        }

        return cardNames;
    }
}

