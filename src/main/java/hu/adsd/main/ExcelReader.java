package hu.adsd.main;

import java.io.*;
import java.util.ArrayList;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/*
* This Class handles all the communications with the local Excel file ExcelSheet.xlsx
* */
public class ExcelReader
{
    // Gets the name of the employee of the Sprint from ExcelSheet.xlsx
    public String EmployeeOfTheSprintName(int vRow, int vColumn)
    {
        //variable for storing the cell value
        String employeeValue = null;
        Workbook workbook = null;

        try {
            File excelFile = new File("ExcelSheet.xlsx");
            //reading data from a file in the form of bytes
            FileInputStream fileInputStream = new FileInputStream(excelFile);

            //constructs an XSSFWorkbook object, by buffering the whole stream into the memory
            workbook = new XSSFWorkbook(fileInputStream);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        //getting the XSSFSheet object at given index
        Sheet sheet = workbook.getSheetAt(0);

        //returns the logical row
        Row row = sheet.getRow(vRow);

        //getting the cell representing the given column
        Cell cell = row.getCell(vColumn);

        //getting cell value
        employeeValue = cell.getStringCellValue();

        return employeeValue;
    }

    // Gets the total storypoints done and total amount of hours worked by the employee of the sprint
    // from ExcelSheet.xlsx
    public ArrayList<Double> EmployeeOfTheSprintInfo(int vRow1, int vRow2, int vColumn)
    {
        Workbook workbook = null;

        try
        {
            File excelFile = new File("ExcelSheet.xlsx");
            //reading data from a file in the form of bytes
            FileInputStream fileInputStream = new FileInputStream(excelFile);

            //constructs an XSSFWorkbook object, by buffering the whole stream into the memory
            workbook = new XSSFWorkbook(fileInputStream);
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (IOException e1)
        {
            e1.printStackTrace();
        }

        //getting the XFFSSheet object at the given index
        Sheet sheet = workbook.getSheetAt(0);

        // getting all rows
        Row row1 = sheet.getRow(vRow1);
        Row row2 = sheet.getRow(vRow2);

        // getting the cells representing the given column
        Cell cell1 = row1.getCell(vColumn);
        Cell cell2 = row2.getCell(vColumn);

        //getting cell values
        Double employeeValue1 = cell1.getNumericCellValue();
        Double employeevalue2 = cell2.getNumericCellValue();

        ArrayList<Double> employeeInfoLost = new ArrayList<>();
        employeeInfoLost.add(employeeValue1);
        employeeInfoLost.add(employeevalue2);

        return employeeInfoLost;
    }

    // Gets the names of all employees working on current sprint from ExcelSheet.xlsx
    public ArrayList<String> Employees (int vRow1, int vRow2, int vRow3, int vRow4, int vRow5, int vRow6, int vColumn)
    {
       Workbook workbook = null;

       try
        {
            File excelFile = new File("ExcelSheet.xlsx");
            //reading data from a file in the form of bytes
            FileInputStream fileInputStream = new FileInputStream(excelFile);

            //constructs an XSSFWorkbook object, by buffering the whole stream into the memory
            workbook = new XSSFWorkbook(fileInputStream);
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (IOException e1)
        {
            e1.printStackTrace();
        }

       //getting the XFFSSheet object at the given index
        Sheet sheet = workbook.getSheetAt(0);

        // getting all rows
        Row row1 = sheet.getRow(vRow1);
        Row row2 = sheet.getRow(vRow2);
        Row row3 = sheet.getRow(vRow3);
        Row row4 = sheet.getRow(vRow4);
        Row row5 = sheet.getRow(vRow5);
        Row row6 = sheet.getRow(vRow6);

        // getting the cells representing the given column
        Cell cell1 = row1.getCell(vColumn);
        Cell cell2 = row2.getCell(vColumn);
        Cell cell3 = row3.getCell(vColumn);
        Cell cell4 = row4.getCell(vColumn);
        Cell cell5 = row5.getCell(vColumn);
        Cell cell6 = row6.getCell(vColumn);

        // getting cell values
        String employeesValue1 = cell1.getStringCellValue();
        String employeesValue2 = cell2.getStringCellValue();
        String employeesValue3 = cell3.getStringCellValue();
        String employeesValue4 = cell4.getStringCellValue();
        String employeesValue5 = cell5.getStringCellValue();
        String employeesValue6 = cell6.getStringCellValue();

        //adding values to arraylist
        ArrayList<String> employeesList = new ArrayList<>();
        employeesList.add(employeesValue1);
        employeesList.add(employeesValue2);
        employeesList.add(employeesValue3);
        employeesList.add(employeesValue4);
        employeesList.add(employeesValue5);
        employeesList.add(employeesValue6);

        return employeesList;
    }

    // Gets the team's schedule from ExcelSheet.xlsx
    public String[][] Scedule(int vRow1, int vRow2, int vRow3, int vRow4, int vRow5, int vRow6, int vRow7, int vColumnNames, int vColumnDay)
    {
        //variable for storing the cell values
        Workbook workbook = null;

        try
        {
            File excelFile = new File("ExcelSheet.xlsx");
            //reading data from a file in the form of bytes
            FileInputStream fileInputStream = new FileInputStream(excelFile);

            //constructs an XSSFWorkbook object, by buffering the whole stream into the memory
            workbook = new XSSFWorkbook(fileInputStream);
        }
        catch (FileNotFoundException e)
        {
            e.printStackTrace();
        }
        catch (IOException e1)
        {
            e1.printStackTrace();
        }
        //getting the XSSFSheet object at given index
        Sheet sheet = workbook.getSheetAt(1);

        // getting all rows
        Row row1 = sheet.getRow(vRow1);
        Row row2 = sheet.getRow(vRow2);
        Row row3 = sheet.getRow(vRow3);
        Row row4 = sheet.getRow(vRow4);
        Row row5 = sheet.getRow(vRow5);
        Row row6 = sheet.getRow(vRow6);
        Row row7 = sheet.getRow(vRow7);

        // getting the cells representing the given column
        Cell cell1 = row1.getCell(vColumnNames);
        Cell cell2 = row2.getCell(vColumnNames);
        Cell cell3 = row3.getCell(vColumnNames);
        Cell cell4 = row4.getCell(vColumnNames);
        Cell cell5 = row5.getCell(vColumnNames);
        Cell cell6 = row6.getCell(vColumnNames);
        Cell cell7 = row7.getCell(vColumnNames);
        Cell cell8 = row1.getCell(vColumnDay);
        Cell cell9 = row2.getCell(vColumnDay);
        Cell cell10 = row3.getCell(vColumnDay);
        Cell cell11 = row4.getCell(vColumnDay);
        Cell cell12 = row5.getCell(vColumnDay);
        Cell cell13 = row6.getCell(vColumnDay);
        Cell cell14 = row7.getCell(vColumnDay);

        // getting cell values
        String sceduleValue1 = cell1.getStringCellValue();
        String sceduleValue2 = cell2.getStringCellValue();
        String sceduleValue3 = cell3.getStringCellValue();
        String sceduleValue4 = cell4.getStringCellValue();
        String sceduleValue5 = cell5.getStringCellValue();
        String sceduleValue6 = cell6.getStringCellValue();
        String sceduleValue7 = cell7.getStringCellValue();
        String sceduleValue8 = cell8.getStringCellValue();
        String sceduleValue9 = cell9.getStringCellValue();
        String sceduleValue10 = cell10.getStringCellValue();
        String sceduleValue11 = cell11.getStringCellValue();
        String sceduleValue12 = cell12.getStringCellValue();
        String sceduleValue13 = cell13.getStringCellValue();
        String sceduleValue14 = cell14.getStringCellValue();

        // adding values to arraylist
        String [][] sceduleValues = {{sceduleValue8, sceduleValue9, sceduleValue10, sceduleValue11, sceduleValue12, sceduleValue13, sceduleValue14},
                {sceduleValue1, sceduleValue2, sceduleValue3, sceduleValue4, sceduleValue5, sceduleValue6, sceduleValue7}};

        return sceduleValues;
    }
}