package hu.adsd.main;

import java.util.ArrayList;

/*
* Object that houses all relevant information for one user story of a scrumboard
* name is the name of the user story
* criteriaAmount is the amount of acceptancecriteria of the user story
* priority is the priority the user story has
* storyPoints is the amount of storypoints the user story is worth
* status is in which column of the scrumboard the user story is
* idMembers is a list of all members connected to this user story
* link is a URL to this specifick user story
* */
public class UserStoryCard
{
    private String name;
    private double criteriaAmount;
    private double criteriaCheck;
    private String priority;
    private int storyPoints;
    private String status;
    private ArrayList<String> idMembers;
    private String link;

    public UserStoryCard(String name, double criteriaAmount, double criteriaCheck, String priority, int storyPoints, String status, ArrayList<String> idMembers, String link) {
		this.name = name;
		this.criteriaAmount = criteriaAmount;
		this.criteriaCheck = criteriaCheck;
		this.priority = priority;
		this.storyPoints = storyPoints;
		this.status = status;
		this.link = link;
        this.status = status;
        this.idMembers = idMembers;
	}

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getCriteriaAmount() {
        return this.criteriaAmount;
    }

    public void setCriteriaAmount(int criteriaAmount) {
        this.criteriaAmount = criteriaAmount;
    }

    public double getCriteriaCheck()
    {
        return this.criteriaCheck;
    }

    public void setCriteriaCheck(int criteriaCheck)
    {
        this.criteriaCheck = criteriaCheck;
    }

    public String getPriority() {
        return this.priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public int getStoryPoints() {
        return this.storyPoints;
    }

    public void setStoryPoints(int storyPoints) {
        this.storyPoints = storyPoints;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(String status) {
		this.status = status;
    }

    public ArrayList<String> getIdMembers() {
        return this.idMembers;
    }

    public void setIdMembers(ArrayList<String> idMembers) {
		this.idMembers = idMembers;
	}

    public String getLink() {
        return this.link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
