package hu.adsd.main;

import java.sql.*;
import java.util.ArrayList;

import org.sqlite.SQLiteDataSource;

/*
* This Class handles all the communications with the local SQLite database
* */
public class DatabaseReader
{
	// Gets one data point for the burndown chart from TrelloApiHandler and puts it in the database scrumboard.db
	// If the database contains 14 data points, all previous data points are erased
	// This method is run once per day at 00:00, regulated in the MainApplication Class
	public void checkChartData()
	{
		TrelloApiHandler trelloApiHandler = new TrelloApiHandler();
		ChartData chartData = trelloApiHandler.getChartFromApi(trelloApiHandler.getIssuesFromAPI());

		try
		{
			SQLiteDataSource dataSource = new SQLiteDataSource();
			dataSource.setUrl("jdbc:sqlite:src/main/resources/scrumboard.db");

			try (Connection connection = dataSource.getConnection())
			{
				String SQLcheckData = "SELECT id FROM chartData WHERE id = (SELECT MAX(id) FROM chartData)";

				try
						(
							Statement statement = connection.createStatement();
							ResultSet resultSet = statement.executeQuery(SQLcheckData)
						)
				{
					if (resultSet.getInt("id") == 14)
					{
						String SQLdeleteData = "DELETE FROM chartData";
						String SQLresetId = "DELETE FROM sqlite_sequence WHERE name='chartData'";

						try (PreparedStatement preparedStatement = connection.prepareStatement(SQLdeleteData))
						{
							preparedStatement.executeUpdate();
						}
						try (PreparedStatement preparedStatement = connection.prepareStatement(SQLresetId))
						{
							preparedStatement.executeUpdate();
						}
					}
				}

				String SQL1 = "INSERT INTO chartData (totalStoryPoints,doneStoryPoints,time) VALUES (?, ?, ?)";

				try (PreparedStatement preparedStatement = connection.prepareStatement(SQL1))
				{
					preparedStatement.setInt(1, chartData.getStoryPointsTotal());
					preparedStatement.setInt(2, chartData.getStoryPointsDone());
					preparedStatement.setString(3, chartData.getStoryPointsDate());

					preparedStatement.executeUpdate();
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	// Gets all data points for use in the burndown chart from the database scrumboard.db
	public ArrayList<ChartData> getChartDataFromDatabase()
	{
		ArrayList<ChartData> chartDataList = new ArrayList<>();

		try
		{
			SQLiteDataSource dataSource = new SQLiteDataSource();
			dataSource.setUrl("jdbc:sqlite:src/main/resources/scrumboard.db");

			String SQL = "SELECT * FROM chartData";

			try (Connection connection = dataSource.getConnection())
			{
				try (Statement statement = connection.createStatement();
					 ResultSet resultSet = statement.executeQuery(SQL))
				{
					while (resultSet.next())
					{
						int totalStoryPoints = resultSet.getInt("totalStoryPoints");
						int doneStoryPoints = resultSet.getInt("doneStoryPoints");
						String time = resultSet.getString("time");

						ChartData chartData = new ChartData(totalStoryPoints, doneStoryPoints, time);
						chartDataList.add(chartData);

					}
				}
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return chartDataList;
	}
}
