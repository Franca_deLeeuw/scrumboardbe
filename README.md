# Scrumdashboard - backend

This is the backend of the Scrumdashboard application made by group 3 of the AD Software Development at the Hogeschool Utrecht.

This backend is a REST API which communicates with the Api's of Google Calendar and Trello, but also with local files like the SQLite database scrumboard.db and the Excel file ExcelSheet.xlsx.

It is made using Java, Spring Boot and uses Gradle for handling dependencies.

While it is running calls can be made to various links starting with http://localhost:8080 to see the results.

This application is specifically made to work with the application located at Scrumdashboard - frontend.

It has been made and should be used only with the approval of all team 3 members, and for educational purposes.